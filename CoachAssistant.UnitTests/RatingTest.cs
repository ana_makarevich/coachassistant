﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CoachAssisant.Models;
using CoachAssisant.Controllers;
using System.Collections.Generic;

namespace CoachAssistant.UnitTests
{
    [TestClass]
    public class RatingTests
    {
        [TestMethod]
        public void GetBestValues_EachMeasurementTypeAddedAsKey()
        {
            // Arrange

            List<MeasurementType> mtypes = getMockMeasurementTypes();
            List<MeasurementRecord> mrecords = getMockMeasurementRecords();
            ClientsRating rating = new ClientsRating(null, mrecords, mtypes);
            // Act
            PrivateObject obj = new PrivateObject(rating);            
            Dictionary<int, BestVal> bestVals = (Dictionary < int, BestVal >) obj.Invoke("getBestValues");
            BestVal test1;
            BestVal test2;
            bestVals.TryGetValue(MeasurementType.Chest, out test1);
            bestVals.TryGetValue(MeasurementType.Neck, out test2);
            // Assert            
            Assert.AreEqual(90, test1.value);
            Assert.AreEqual(90, test2.value);            
        }

        [TestMethod]
        public void ClientsTotal_SingleClient_ReturnsNumberOfMeasurements()
        {
            // Arrange
            List<MeasurementType> mtypes = getMockMeasurementTypes();
            List<MeasurementRecord> mrecords = getMockMeasurementRecords();
            ClientsRating rating = new ClientsRating(null, mrecords, mtypes);
            // Act
            PrivateObject obj = new PrivateObject(rating);            
            float retVal = (float)obj.Invoke("getClientsTotal", mrecords);
            // Assert            
            Assert.AreEqual(2, retVal);
        }

        private List<MeasurementType> getMockMeasurementTypes()
        {
            MeasurementType neck = new MeasurementType
            {
                Id = MeasurementType.Chest,
                IsToBeMinimised = true,
            };
            MeasurementType waist = new MeasurementType
            {
                Id = MeasurementType.Neck,
                IsToBeMinimised = true,
            };

            List<MeasurementType> mtypes = new List<MeasurementType> { neck, waist };
            return mtypes;
        }
        private List<MeasurementRecord> getMockMeasurementRecords()
        {
            MeasurementRecord mrecord1 = new MeasurementRecord
            {
                ClientId = 1002,
                MeasurementTypeId = MeasurementType.Chest,
                Date = DateTime.Now,
                Value = 90
            };
            MeasurementRecord mrecord2 = new MeasurementRecord
            {
                ClientId = 1002,
                MeasurementTypeId = MeasurementType.Neck,
                Date = DateTime.Now,
                Value = 90
            };
            List<MeasurementRecord> mrecords = new List<MeasurementRecord> { mrecord1, mrecord2 };
            return mrecords;

        }
    }
}