# CoachAssistant

## RU

Этот проект был создан в рамках курсовой работы по курсу "Программирование под платформу .NET". Основан на ASP.NET MVC 5 и Entity Framework 6. 
Приложение представляет собой сервис для персональных тренеров. Он позволяет вести учет тренировок и платежей, а также анализировать прогресс клиентов. 
В дополнение к основному функционалу реализован базовый API, который может быть использован при создании мобильного клиента. 

## EN

This project has been created as a course project for the course ".NET programming". Based on ASP.NET MVC 5 and Entity Framework 6.
The app is designed for the personal trainers. It allows to keep records of the workouts and payments and and also to analyse clients' progress.
It also has a basic API which can be used by a mobile client. 
