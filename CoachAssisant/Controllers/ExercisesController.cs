﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoachAssisant.Models;
using System.Data.Entity;
using CoachAssisant.Models.ViewModels;

namespace CoachAssisant.Controllers
{
    public class ExercisesController : Controller
    {
        private ApplicationDbContext _context;
        public ExercisesController()
        {
            _context = new ApplicationDbContext();

        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }


        public ActionResult Index(int? pageIndex, string sortBy)
        {
            if (!pageIndex.HasValue)
                pageIndex = 1;
            if (String.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "Name";
            }
            var exercises = _context.Exercises.ToList();
            
            return View(exercises);            
        }
        // GET: This is action for tests
        public ActionResult Random()
        {

            var v1 = DateTime.Now.ToString("yyyyMM");
            var v2 = DateTime.Now.ToString("yyyymmddd");
            var v3 = String.Format("yyyymm", DateTime.Now);
            return Content("yyyymm=" + v1 + ", yyyymmdd=" + v2 + ", yearmonth=" + v3);
        }

        // custom routing
        [Route("exercises/added/{year:regex(\\d{4})}/{month:regex(\\d{2}):range(1,12)}")]
        public ActionResult ByAddedDate(int year, int month)
        {
            return Content(year + "/" + month);
        }

        public ActionResult New()
        {
            Exercise exercise = new Exercise();
            ViewBag.Title = "Добавление упражнения";
            ViewBag.ButtonText = "Добавить";
            return View("ExerciseForm", exercise);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Exercise exercise)
        {
         
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                string result = "";
                foreach (ModelError er in errors)
                {
                    result += er.ErrorMessage;
                }

                ViewBag.Title = "Добавление упражнения 1";
                ViewBag.Errors = result;

                if (exercise.Id == 0)
                {
                    
                    ViewBag.ButtonText = "Добавить";
                }
                else
                {
                    ViewBag.Title = "Изменение упражнения 1";
                    ViewBag.ButtonText = "Изменить";
                }
                return View("ExerciseForm", exercise);
            }
       
            if (exercise.Id == 0)
            {
                _context.Exercises.Add(exercise);
            }
            else
            {
                var exerciseInDb = _context.Exercises.Single(e => e.Id == exercise.Id);
                exerciseInDb.Name = exercise.Name;
                exerciseInDb.AlternativeName = exercise.AlternativeName;
                exerciseInDb.TargetMuscles = exercise.TargetMuscles;
            }
            
            _context.SaveChanges();
            return RedirectToAction("Index", "Exercises");
        }

        public ActionResult Edit(int id)
        {
            var exercise = _context.Exercises.SingleOrDefault(c => c.Id == id);
            if (exercise == null)
            {
                return HttpNotFound();
            }
            ViewBag.Title = "Изменение упражнения";
            ViewBag.ButtonText = "Изменить";
            return View("ExerciseForm", exercise);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var exercise = _context.Exercises.SingleOrDefault(c => c.Id == id);
            if (exercise == null)
            {
                return HttpNotFound();
            }
            return View(exercise);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var exercise = _context.Exercises.SingleOrDefault(c => c.Id == id);
            if (exercise == null)
            {
                return HttpNotFound();
            }
            _context.Exercises.Remove(exercise);
            _context.SaveChanges();
            return RedirectToAction("Index");

        }
    }
}