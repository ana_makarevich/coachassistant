﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoachAssisant.Models;
using CoachAssisant.ViewModels;
using System.Data.Entity;
namespace CoachAssisant.Controllers
{
    public class PaymentsController : Controller
    {
        private ApplicationDbContext _context;
        public PaymentsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Payments
        public ActionResult Index()
        {
            var payments = _context.Payments.Include(c => c.Client).ToList();
            return View(payments);
        }

        public ActionResult New()
        {
            var clients = _context.Clients.ToList();
            var payment = new Payment();
            payment.Date = DateTime.Now;
            var viewModel = new PaymentFormViewModel
            {
                Clients = clients,
                Payment = payment
            };

            return View("PaymentForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Payment payment)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new PaymentFormViewModel
                {
                    Clients = _context.Clients.ToList(),
                    Payment = payment
                };
                return View("PaymentForm", viewModel);
            }
            payment.Date = DateTime.Now;
            if (payment.Id == 0)
            {
                _context.Payments.Add(payment);
            }
            else
            {
                var paymentInDb = _context.Payments.Single(p => p.Id == payment.Id);
                paymentInDb.Amount = payment.Amount;
                paymentInDb.ClientId = payment.ClientId;
                paymentInDb.OnTime = payment.OnTime;
                paymentInDb.Notes = payment.Notes;
                paymentInDb.Date = payment.Date;
            }
            
            _context.SaveChanges();
            return RedirectToAction("Index", "Payments");
        }

        public ActionResult Edit(int id)
        {
            var payment = _context.Payments.SingleOrDefault(c => c.Id == id);
            if (payment == null)
            {
                return HttpNotFound();
            }
            var viewModel = new PaymentFormViewModel
            {
                Clients = _context.Clients.ToList(),
                Payment = payment
            };

            return View("PaymentForm", viewModel);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var payment = _context.Payments.SingleOrDefault(c => c.Id == id);
            if (payment == null)
            {
                return HttpNotFound();
            }
            return View(payment);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var payment = _context.Payments.SingleOrDefault(c => c.Id == id);
            if (payment == null)
            {
                return HttpNotFound();
            }
            _context.Payments.Remove(payment);
            _context.SaveChanges();
            return RedirectToAction("Index");

        }
    }
}