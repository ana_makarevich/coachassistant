﻿using CoachAssisant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using CoachAssisant.ViewModels;

namespace CoachAssisant.Controllers
{
    public class AchievmentController : Controller
    {
        private ApplicationDbContext _context;
        public AchievmentController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Achievment
        
        public ActionResult Index()
        {            
            if (User.IsInRole(RoleName.CanManageClients))
            {
                return View("Index");
            }            
            return View("ReadOnlyList");
        }    
        public ActionResult AchievmentsByMonth(int yearmonth)
        {
            return Content("Start=" + yearmonth);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Achievment achievment)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new AchievmentViewFormModel
                {
                    Achievment = achievment,
                    Categories = _context.Categories.ToList(),
                    Clients = _context.Clients.ToList()
                };
                if (achievment.Id == 0)
                {
                    ViewBag.Title = "Добавление достижения";
                    ViewBag.ButtonText = "Добавить";
                }
                else
                {
                    ViewBag.Title = "Редактирование достижения";
                    ViewBag.ButtonText = "Изменить";
                }
                return View("AchievmentForm", viewModel);
            }
            achievment.yearmonth = Int32.Parse(achievment.Added.ToString("yyyyMM"));
            if (achievment.Id == 0)
            {
                _context.Achievments.Add(achievment);
            }
            else
            {
                var achievmentInDb = _context.Achievments.Single(a => a.Id == achievment.Id);
                achievmentInDb.yearmonth = achievment.yearmonth;
                achievmentInDb.Added = achievment.Added;
                achievmentInDb.CategoryId = achievment.CategoryId;
                achievmentInDb.ClientId = achievment.ClientId;
                achievmentInDb.Description = achievment.Description;

            }
            _context.SaveChanges();

            return RedirectToAction("Index", "Achievment");
        }
        [Authorize(Roles = RoleName.CanManageClients)]
        public ActionResult New()
        {
            var categories = _context.Categories.ToList();
            var clients = _context.Clients.ToList();
            var achievment = new Achievment();
            achievment.Added = DateTime.Now;
            var viewModel = new AchievmentViewFormModel
            {
                Categories = categories,
                Clients = clients,
                Achievment = achievment
            };
            ViewBag.Title = "Добавление достижения";
            ViewBag.ButtonText = "Добавить";
            return View("AchievmentForm", viewModel);
        }

        [Authorize(Roles = RoleName.CanManageClients)]
        public ActionResult Edit(int id)
        {
            var achievment = _context.Achievments.SingleOrDefault(c => c.Id == id);
            if (achievment == null)
            {
                return HttpNotFound();
            }
            var viewModel = new AchievmentViewFormModel
            {
                Categories = _context.Categories.ToList(),
                Clients = _context.Clients.ToList(),
                Achievment = achievment
            };
            ViewBag.Title = "Редактирование достижения";
            ViewBag.ButtonText = "Изменить";
            return View("AchievmentForm", viewModel);            
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var achievment = _context.Achievments.SingleOrDefault(c => c.Id == id);
            if (achievment == null)
            {
                return HttpNotFound();
            }
            return View(achievment);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var achievment = _context.Achievments.SingleOrDefault(c => c.Id == id);
            if (achievment == null)
            {
                return HttpNotFound();
            }
            _context.Achievments.Remove(achievment);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }


    }
}