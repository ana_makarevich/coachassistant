﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoachAssisant.Models;
using CoachAssisant.ViewModels;
using System.Data.Entity;

namespace CoachAssisant.Controllers
{
    public class MeasurementRecordsController : Controller
    {
        private ApplicationDbContext _context;
        // GET: Measurements
        public MeasurementRecordsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        public ActionResult Index()
        {
            var measurements = _context.MeasurementRecords.
                Include(c => c.MeasurementType).
                Include(c => c.Client).
                ToList();
            return View();
        }
        public ActionResult New()
        {
            var measurementTypes = _context.MeasurementTypes.ToList();
            var clients = _context.Clients.ToList();
            var measurementRecord = new MeasurementRecord();
            measurementRecord.Date = DateTime.Now;
            var viewModel = new MeasurementRecordFormViewModel
            {
                MeasurementTypes = measurementTypes,
                Clients = clients,
                MeasurementRecord = measurementRecord
            };
            return View("MeasurementRecordForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(MeasurementRecord measurementRecord)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new MeasurementRecordFormViewModel
                {
                    MeasurementTypes = _context.MeasurementTypes.ToList(),
                    Clients = _context.Clients.ToList(),
                    MeasurementRecord = measurementRecord
                };
                return View("MeasurementRecordForm", viewModel);
            }

            if (measurementRecord.Id == 0)
            {
                // TODO: add calculating if it's an improvement
                _context.MeasurementRecords.Add(measurementRecord);
            }
            else
            {
                var measurementRecordInDb = _context.MeasurementRecords.Single(m => m.Id == measurementRecord.Id);
                measurementRecordInDb.ClientId = measurementRecord.ClientId;
                measurementRecordInDb.MeasurementTypeId = measurementRecord.MeasurementTypeId;
                measurementRecordInDb.Date = measurementRecord.Date;
                measurementRecordInDb.Value = measurementRecord.Value;
                
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "MeasurementRecords");
        }

        public ActionResult Edit(int id)
        {
            var measurementRecord = _context.MeasurementRecords.SingleOrDefault(c => c.Id == id);
            if (measurementRecord == null)
            {
                return HttpNotFound();
            }
            var viewModel = new MeasurementRecordFormViewModel
            {
                MeasurementTypes = _context.MeasurementTypes.ToList(),
                MeasurementRecord = measurementRecord
            };

            return View("MeasurementRecordForm", viewModel);

        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var measurementRecord = _context.MeasurementRecords.SingleOrDefault(c => c.Id == id);
            if (measurementRecord == null)
            {
                return HttpNotFound();
            }
            return View(measurementRecord);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var measurementRecord = _context.MeasurementRecords.SingleOrDefault(c => c.Id == id);
            if (measurementRecord == null)
            {
                return HttpNotFound();
            }
            _context.MeasurementRecords.Remove(measurementRecord);
            _context.SaveChanges();
            return RedirectToAction("Index");

        }

    }
}