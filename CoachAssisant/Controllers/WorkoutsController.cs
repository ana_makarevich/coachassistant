﻿using CoachAssisant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using CoachAssisant.ViewModels;


namespace CoachAssisant.Controllers
{
    public class WorkoutsController : Controller
    {
        private ApplicationDbContext _context;
        public WorkoutsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Workouts
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.Client, Duration = 50)]
        public ActionResult Index()
        {
            var workouts = _context.Workouts.Include(c => c.Client).
                Include(c => c.WorkoutStatus).
                Include(c => c.WokroutType).ToList();

            return View(workouts);
        }

        public ActionResult New()
        {
            var workoutTypes = _context.WorkoutTypes.ToList();
            var workoutStatuses = _context.WorkoutStatuses.ToList();
            var clients = _context.Clients.ToList();
            var workout = new Workout();
            workout.Date = DateTime.Now;
            var viewModel = new WorkoutFormViewModel
            {
                WorkoutStatuses = workoutStatuses,
                WorkoutTypes = workoutTypes,
                Clients = clients,
                Workout = workout

            };
            return View("WorkoutForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Workout workout)
        {

            if (!ModelState.IsValid)
            {
                var viewModel = new WorkoutFormViewModel
                {
                    Workout = workout,
                    Clients = _context.Clients.ToList(),
                    WorkoutStatuses = _context.WorkoutStatuses.ToList(),
                    WorkoutTypes = _context.WorkoutTypes.ToList()
                };

                return View("WorkoutForm", viewModel);
            }
            if (workout.Id == 0)
            {
                // TODO: write logic for getting the next workout number
                _context.Workouts.Add(workout);
            }
            else
            {
                var workoutInDb = _context.Workouts.Single(w => w.Id == workout.Id);
                workoutInDb.ClientId = workout.ClientId;
                workoutInDb.Date = workout.Date;
                workoutInDb.WorkoutStatusID = workout.WorkoutStatusID;
                workoutInDb.WorkoutTypeId = workout.WorkoutTypeId;
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "Workouts");
        }

        public ActionResult Edit(int id)
        {
            var workout = _context.Workouts.SingleOrDefault(c => c.Id == id);
            if (workout == null)
            {
                return HttpNotFound();
            }
            var viewModel = new WorkoutFormViewModel
            {
                WorkoutStatuses = _context.WorkoutStatuses.ToList(),
                WorkoutTypes = _context.WorkoutTypes.ToList(),
                Clients = _context.Clients.ToList(),
                Workout = workout
            };

            return View("WorkoutForm", viewModel);
        }

        public ActionResult Details(int id)
        {

            var workout = _context.Workouts.Include(c => c.WorkoutStatus).
                Include(c => c.WokroutType).Include(c => c.Client).
                SingleOrDefault(w => w.Id == id);
            var exercises = _context.WorkoutExercises.Where(c => c.WorkoutId == id).
                Include(c => c.Exercise);
            if (workout == null)
            {
                return HttpNotFound();
            }
            var viewModel = new WorkoutDetailsViewModel
            {
                Workout = workout,
                WorkoutExercises = exercises
            };
            ViewBag.Title = "Тренировка";
            return View("Details", viewModel);

        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var workout = _context.Workouts.SingleOrDefault(w => w.Id == id);
            if (workout == null)
            {
                return HttpNotFound();
            }
            return View(workout);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var workout = _context.Workouts.SingleOrDefault(w => w.Id == id);
            if (workout == null)
            {
                return HttpNotFound();
            }
            _context.Workouts.Remove(workout);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}