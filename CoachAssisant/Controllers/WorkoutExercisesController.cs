﻿using CoachAssisant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using CoachAssisant.ViewModels;
using CoachAssisant.Dtos;

namespace CoachAssisant.Controllers
{
    public class WorkoutExercisesController : Controller
    {

        private ApplicationDbContext _context;
        public WorkoutExercisesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult New(int? workoutId, int? clientId)
        {
            /*
             * change so that we check the argument passed here
             * 
             * **/            
            var exerciseTypes = _context.Exercises.ToList();

            if (workoutId.HasValue)
            {
                var workoutExercise = new WorkoutExercise();
                Workout workout = _context.Workouts.SingleOrDefault(w => w.Id == workoutId.Value);
                workoutExercise.WorkoutId = workoutId.Value;
                var viewModel = new WorkoutExerciseFormViewModel
                {
                    Exercises = exerciseTypes,
                    WorkoutExercise = workoutExercise,
                    WorkoutId = workoutId.Value
                };
                return View("WorkoutExerciseForm", viewModel);
            }                
            else if (clientId.HasValue)
            {
                return View("New");

            }                        
            else
            {
                return new HttpNotFoundResult();
            }
                                                

            
        }
        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(WorkoutExercise workoutExercise)
        {
            if (!ModelState.IsValid){
                var viewModel = new WorkoutExerciseFormViewModel
                {
                    WorkoutExercise = workoutExercise,
                    Exercises = _context.Exercises.ToList()
                };
                return View("WorkoutExerciseForm", viewModel);



            }
            if (workoutExercise.Id == 0)
            {
                _context.WorkoutExercises.Add(workoutExercise);
            }
            else
            {
                var workoutExerciseInDb = _context.WorkoutExercises.Single(e => e.Id == workoutExercise.Id);
                workoutExerciseInDb.ExerciseId = workoutExercise.ExerciseId;
                workoutExerciseInDb.WorkoutId = workoutExercise.WorkoutId;
                workoutExerciseInDb.Wraps = workoutExercise.Wraps;
                workoutExerciseInDb.NumberOfSets = workoutExercise.NumberOfSets;
                workoutExercise.WorkingWeight = workoutExercise.WorkingWeight;
                workoutExerciseInDb.AdditionalName = workoutExercise.AdditionalName;
            }
            _context.SaveChanges();
            // TODO: change to specific workout
            return RedirectToAction("Details", new { controller = "Workouts",
                action = "Details", Id = workoutExercise.WorkoutId });
        }

        public ActionResult Edit(int id)
        {
            var workoutExercise = _context.WorkoutExercises.SingleOrDefault(e => e.Id == id);            
            if (workoutExercise == null)
            {
                return HttpNotFound();
            }
            var viewModel = new WorkoutExerciseFormViewModel
            {                
                Exercises = _context.Exercises.ToList(),
                WorkoutExercise = workoutExercise
            };

            return View("WorkoutExerciseForm", viewModel);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var workoutExercise = _context.WorkoutExercises.SingleOrDefault(e => e.Id == id);
            if (workoutExercise == null)
            {
                return HttpNotFound();
            }
            return View(workoutExercise);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var workoutExercise = _context.WorkoutExercises.SingleOrDefault(e => e.Id == id);
            if (workoutExercise == null)
            {
                return HttpNotFound();
            }
            _context.WorkoutExercises.Remove(workoutExercise);
            _context.SaveChanges();
            return RedirectToAction("Details", new { controller = "Workouts",
                action = "Details", Id = workoutExercise.WorkoutId });

        }
        


    }
}