﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CoachAssisant.Dtos;
using CoachAssisant.Models;
using AutoMapper;
using System.Data.Entity;

namespace CoachAssisant.Controllers.Api
{
    public class NewWorkoutExerciseController : ApiController
    {
        private ApplicationDbContext _context;

        public NewWorkoutExerciseController()
        {
            _context = new ApplicationDbContext();
        }
        [HttpPost]
        public IHttpActionResult CreateNewWorkoutExercise(WorkoutExerciseDto newWorkoutExercise)
        {

            // check if any exercises were selected
            if (newWorkoutExercise.ExercisesIds.Count == 0)
                return BadRequest("Упражнения не выбраны");

            // check if client exists
            var client = _context.Clients.SingleOrDefault(
                c => c.Id == newWorkoutExercise.ClientId);
            if (client == null)
                return BadRequest("Пользователя не существует!");
            
            // try to find today's workout with  this client -> if it exists, we should add 
            // exercises to already existing workout
            var selectedWorkout = _context.Workouts.SingleOrDefault(workout => 
                workout.ClientId == client.Id & workout.Date.Date == DateTime.Today);

            // if it's not found -> add new one so that the user doesn't have to go and add manually
            if (selectedWorkout == null)
            {
                selectedWorkout = new Workout
                {
                    Date = DateTime.Today,
                    ClientId = newWorkoutExercise.ClientId,
                    WokroutType = null,
                    WorkoutStatus = null

                };
                _context.Workouts.Add(selectedWorkout);
                _context.SaveChanges();
            }   
            
            // find all selected exercises in the database
            var exercises = _context.Exercises.Where(
                exercise => newWorkoutExercise.ExercisesIds.Contains(exercise.Id)).ToList();

            foreach (var exercise in exercises)
            {
                var workoutExercise = new WorkoutExercise
                {
                    Workout = selectedWorkout,
                    WorkoutId = selectedWorkout.Id,
                    Exercise = exercise,
                    ExerciseId = exercise.Id,
                    WorkingWeight = 0,
                    Wraps = 0,
                    NumberOfSets = 0
                };
                _context.WorkoutExercises.Add(workoutExercise);
            }
            _context.SaveChanges();
            return Ok();
        }
    }
}
