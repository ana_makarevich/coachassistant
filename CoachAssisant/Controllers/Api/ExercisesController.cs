﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CoachAssisant.Models;
using CoachAssisant.Dtos;
using CoachAssisant.App_Start;
using AutoMapper;

namespace CoachAssisant.Controllers.Api
{
    public class ExercisesController : ApiController
    {
        private ApplicationDbContext _context;
        public ExercisesController()
        {
            _context = new ApplicationDbContext();
        }                            

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET /api/exercises
        public IHttpActionResult GetExercises(string query = null)
        {
            IEnumerable<Exercise> exercisesQuery = _context.Exercises.ToList();

            if (!String.IsNullOrWhiteSpace(query))
                exercisesQuery = exercisesQuery.Where(c => c.Name.Contains(query));

            var exercisesDtos = exercisesQuery.Select(Mapper.Map<Exercise, ExerciseDto>);

            return Ok(exercisesDtos);
        }

        // GET /api/exercises/1
        public IHttpActionResult GetExercise(int id)
        {
            var exercise = _context.Exercises.SingleOrDefault(c => c.Id == id);
            if (exercise == null)
                return NotFound();

            return Ok(Mapper.Map<Exercise, ExerciseDto>(exercise));
        }

        // POST /api/exercises
        [HttpPost]
        public IHttpActionResult CreateExercise(ExerciseDto exerciseDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var exercise = Mapper.Map<ExerciseDto, Exercise>(exerciseDto);
            _context.Exercises.Add(exercise);
            _context.SaveChanges();
            exerciseDto.Id = exercise.Id;
            return Created(new Uri(Request.RequestUri + "/" + exercise.Id),exerciseDto);                
        }

        // PUT /api/exercises/1
        [HttpPut]
        public void UpdateExercise(int id, ExerciseDto exerciseDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var exerciseInDb = _context.Exercises.SingleOrDefault(c => c.Id == id);
            if (exerciseInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            Mapper.Map<ExerciseDto, Exercise>(exerciseDto, exerciseInDb);            
            _context.SaveChanges();
        }

        // DELETE /api/exercises/1
        [HttpDelete]
        public void DeleteExercise(int id)
        {
            var exerciseInDb = _context.Exercises.SingleOrDefault(c => c.Id == id);
            if (exerciseInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            _context.Exercises.Remove(exerciseInDb);
            _context.SaveChanges();

        }

    }
}
