﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CoachAssisant.Models;
using CoachAssisant.Dtos;
using CoachAssisant.App_Start;
using AutoMapper;
using System.Data.Entity;

namespace CoachAssisant.Controllers.Api
{
    public class AchievmentsController : ApiController
    {

        private ApplicationDbContext _context;
        public AchievmentsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET /api/exercises
        public IEnumerable<AchievmentDto> GetAchievments()
        {
            return _context.Achievments.
                Include(c => c.Category).
                Include(c => c.Client).
                ToList().Select(Mapper.Map<Achievment, AchievmentDto>);
        }

        // GET /api/exercises/1
        public IHttpActionResult GetAchievment(int id)
        {
            var exercise = _context.Achievments.SingleOrDefault(c => c.Id == id);
            if (exercise == null)
                return NotFound();

            return Ok(Mapper.Map<Achievment, AchievmentDto>(exercise));
        }

        // POST /api/exercises
        [HttpPost]
        public IHttpActionResult CreateAchievment(AchievmentDto exerciseDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var exercise = Mapper.Map<AchievmentDto, Achievment>(exerciseDto);
            _context.Achievments.Add(exercise);
            _context.SaveChanges();
            exerciseDto.Id = exercise.Id;
            return Created(new Uri(Request.RequestUri + "/" + exercise.Id), exerciseDto);
        }

        // PUT /api/exercises/1
        [HttpPut]
        public void UpdateAchievment(int id, AchievmentDto exerciseDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var exerciseInDb = _context.Achievments.SingleOrDefault(c => c.Id == id);
            if (exerciseInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            Mapper.Map<AchievmentDto, Achievment>(exerciseDto, exerciseInDb);
            _context.SaveChanges();
        }

        // DELETE /api/exercises/1
        [HttpDelete]
        public void DeleteAchievment(int id)
        {
            var exerciseInDb = _context.Achievments.SingleOrDefault(c => c.Id == id);
            if (exerciseInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            _context.Achievments.Remove(exerciseInDb);
            _context.SaveChanges();

        }
    }
}
