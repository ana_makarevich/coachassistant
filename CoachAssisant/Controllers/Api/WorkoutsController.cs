﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CoachAssisant.Models;
using CoachAssisant.Dtos;
using CoachAssisant.App_Start;
using AutoMapper;
using System.Data.Entity;

namespace CoachAssisant.Controllers.Api
{
    public class WorkoutsController : ApiController
    {
        private ApplicationDbContext _context;

        public WorkoutsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET /api/workouts
        public IEnumerable<WorkoutDto> Getworkouts()
        {
            return _context.Workouts.
                Include(m => m.WorkoutStatus).
                Include(m => m.WokroutType).
                Include(m => m.Client).
                ToList().Select(Mapper.Map<Workout, WorkoutDto>);            
        }

        [Route("api/workouts/{year:regex(\\d{4})}/{month:regex(\\d{2}):range(1,12)}")]
        [HttpGet]
        public IEnumerable<WorkoutDto> ByDate(int year, int month)
        {
            
            var workouts = _context.Workouts.
                Include(m => m.WorkoutStatus).
                Include(m => m.WokroutType).
                Include(m => m.Client).Where(c => (c.Date.Year == year) && (c.Date.Month == month));
            return workouts.ToList().Select(Mapper.Map<Workout, WorkoutDto>);

        }

            // GET /api/workouts/1
        public IHttpActionResult Getworkout(int id)
        {
            var workout = _context.Workouts.SingleOrDefault(c => c.Id == id);
            if (workout == null)
                return NotFound();

            return Ok(Mapper.Map<Workout, WorkoutDto>(workout));
        }

        // POST /api/workouts
        [HttpPost]
        public IHttpActionResult Createworkout(WorkoutDto workoutDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var workout = Mapper.Map<WorkoutDto, Workout>(workoutDto);
            _context.Workouts.Add(workout);
            _context.SaveChanges();
            workoutDto.Id = workout.Id;
            return Created(new Uri(Request.RequestUri + "/" + workout.Id),workoutDto);                
        }

        // PUT /api/workouts/1
        [HttpPut]
        public void Updateworkout(int id, WorkoutDto workoutDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var workoutInDb = _context.Workouts.SingleOrDefault(c => c.Id == id);
            if (workoutInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            Mapper.Map<WorkoutDto, Workout>(workoutDto, workoutInDb);            
            _context.SaveChanges();
        }

        // DELETE /api/workouts/1
        [HttpDelete]
        public void Deleteworkout(int id)
        {
            var workoutInDb = _context.Workouts.SingleOrDefault(c => c.Id == id);
            if (workoutInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            _context.Workouts.Remove(workoutInDb);
            _context.SaveChanges();

        }

    }
}
