﻿using CoachAssisant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using CoachAssisant.Dtos;
using AutoMapper;

namespace CoachAssisant.Controllers.Api
{
    public class ClientsController : ApiController
    {
        private ApplicationDbContext _context;

        public ClientsController()
        {
            _context = new ApplicationDbContext();
        }

        public IHttpActionResult GetClients(string query = null)
        {
            IEnumerable<Client> clientsQuery = _context.Clients.Include(c => c.Coach).ToList();

            if (!String.IsNullOrWhiteSpace(query))
                clientsQuery = clientsQuery.Where(c => c.FullName.Contains(query));

            var clientDtos = clientsQuery.Select(Mapper.Map<Client, ClientDto>);
            return Ok(clientDtos);
        }

        [HttpPost]
        public IHttpActionResult CreateClient(ClientDto clientDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var client = Mapper.Map<ClientDto, Client>(clientDto);
            _context.Clients.Add(client);
            _context.SaveChanges();

            clientDto.Id = client.Id;
            return Created(new Uri(Request.RequestUri + "/" + client.Id), clientDto);
        }

        [HttpPut]
        public IHttpActionResult UpdateClient(int id, ClientDto clientDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var clientInDb = _context.Clients.SingleOrDefault(c => c.Id == id);

            if (clientInDb == null)
                return NotFound();

            Mapper.Map(clientDto, clientInDb);
            _context.SaveChanges();

            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult DeleteClient(int id)
        {
            var clientInDb = _context.Clients.SingleOrDefault(c => c.Id == id);

            if (clientInDb == null)
                return NotFound();

            _context.Clients.Remove(clientInDb);
            _context.SaveChanges();

            return Ok();
        }
    }
}
