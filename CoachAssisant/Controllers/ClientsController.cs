﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoachAssisant.Models;
using System.Data.Entity;
using CoachAssisant.ViewModels;


namespace CoachAssisant.Controllers
{
    [Authorize(Roles = RoleName.CanManageClients)]
    public class ClientsController : Controller
    {
        private ApplicationDbContext _context;
        public ClientsController()
        {
            // disposable object!!!
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult New()        
        {
            var client = new Client();
            client.BirthDate = DateTime.Now;
            ViewBag.Title = "Добавление клиента";
            ViewBag.ButtonText = "Добавить";
            return View("ClientForm", client);
        }
        // GET: Clients
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Client client)
        {
            if (!ModelState.IsValid)
            {
                if (client.Id == 0)
                {
                    // define title and button name in case validation fails
                    ViewBag.Title = "Добавление клиента";
                    ViewBag.ButtonText = "Добавить";
                }
                else
                {
                    // define title and button name in case validation fails
                    ViewBag.Title = "Изменение клиента";
                    ViewBag.ButtonText = "Изменить";
                }
                return View("ClientForm", client);
            }
            DateTime birthDate = client.BirthDate;
            client.birthdate_yyyymmdd = Int32.Parse(birthDate.ToString("yyyyMMdd"));                        
            if (client.Id == 0)
            {
                // set default values for the new client
                client.WorkoutsCount = 0;
                client.Reputation = 0;
                _context.Clients.Add(client);
            }            
            else
            {
                // manually update all fields because this is less prone to injections
                var clientInDb = _context.Clients.Single(c => c.Id == client.Id);
                clientInDb.FirstName = client.FirstName;
                clientInDb.LastName = client.LastName;
                clientInDb.BirthDate = client.BirthDate;
                clientInDb.birthdate_yyyymmdd = client.birthdate_yyyymmdd;
                clientInDb.IsActive = client.IsActive;
                clientInDb.Weight = client.Weight;
                clientInDb.Email = client.Email;
            }

            _context.SaveChanges();
            return RedirectToAction("Index", "Clients"); 

        }
        public ActionResult Index()
        {

            // deffered execution! actually exectuded when called in view
            //var clients = _context.Clients;
            // immediate execution:
            var clients = _context.Clients.Include(c => c.Coach).ToList();            
            return View(clients);
            
        }

        public ActionResult Details(int id)
        {
            //also executed immediately
            var client = _context.Clients.Include(c => c.Coach).SingleOrDefault(c => c.Id == id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        public ActionResult Edit(int id)
        {
            var client = _context.Clients.SingleOrDefault(c => c.Id == id);
            if (client == null)
            {
                return HttpNotFound();
            }
            ViewBag.Title = "Изменение клиента";
            ViewBag.ButtonText = "Изменить";
            return View("ClientForm", client);

        }

        public ActionResult Rating()
        {            
            List<Client> clients = _context.Clients.ToList();
            List<MeasurementRecord> mrecords = _context.MeasurementRecords.ToList();
            List<MeasurementType> mtypes = _context.MeasurementTypes.ToList();
            ClientsRating rating = new ClientsRating(clients, mrecords, mtypes);
            var viewModel = rating.getRating();

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var client = _context.Clients.Include(c => c.Coach).SingleOrDefault(c => c.Id == id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var client = _context.Clients.Include(c => c.Coach).SingleOrDefault(c => c.Id == id);
            if (client == null)
            {
                return HttpNotFound();
            }
            _context.Clients.Remove(client);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }

}