﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoachAssisant.ViewModels;
using CoachAssisant.Models;

namespace CoachAssisant.Controllers
{
    public struct BestVal
    {
        public float value;
        public bool min;
        public BestVal(float val, bool m)
        {
            value = val;
            min = m;
        }
    }

    public class ClientsRating
    {
        List<Client> clients;
        List<MeasurementRecord> mrecords;
        List<MeasurementType> mtypes;
        Dictionary<int, BestVal> bestValues;

        public ClientsRating(List<Client> clients, List<MeasurementRecord> mrecords, List<MeasurementType> mtypes)
        {
            this.clients = clients;
            this.mrecords = mrecords;
            this.mtypes = mtypes;
            this.bestValues = getBestValues();
        }

        private float getBestValue(MeasurementType mtype, IEnumerable<MeasurementRecord> mrecords) 
        {
            if (mtype.IsToBeMinimised)
            {
                return mrecords.Count() > 0 ? mrecords.Min(m => m.Value) : 0.0f;
            }
            return mrecords.Count() > 0 ? mrecords.Max(m => m.Value) : 0.0f;

        }

        public float getClientsTotal(IEnumerable<MeasurementRecord> records)
        {
            float value = 0.0f;
            foreach (KeyValuePair<int, BestVal> kvp in bestValues)
            {
                float relative_value = 0.0f;
                if (kvp.Value.min)
                {
                    relative_value = kvp.Value.value / records.Average(m => m.Value);
                }
                else
                {
                    relative_value = records.Average(m => m.Value) / kvp.Value.value;
                }
                value += relative_value;
            }
            return value;
        }

        protected Dictionary<int,BestVal> getBestValues()
        {
            Dictionary<int, BestVal> bestValues = new Dictionary<int, BestVal>();
            foreach (MeasurementType mtype in mtypes)
            {

                var relevantRecords = mrecords.Where(m => m.MeasurementTypeId == mtype.Id);
                float best = getBestValue(mtype, relevantRecords);
                if (best > 0)
                {
                    bestValues.Add(mtype.Id, new BestVal(best, mtype.IsToBeMinimised));

                }
            }
            return bestValues;
        }

        private List<RatingViewModel> getRatingViewModel()
        {
            List<RatingViewModel> viewModel = new List<RatingViewModel>();

            foreach (Client client in clients)
            {
                // select record                                 
                var records = mrecords.Where(m => m.ClientId == client.Id);
                float value = getClientsTotal(records);

                RatingViewModel model = new RatingViewModel { Client = client, Value = value };
                viewModel.Add(model);

            }
            viewModel = viewModel.OrderByDescending(o => o.Value).ToList();
            return viewModel;
        }
        public List<RatingViewModel> getRating()
        {
            List<RatingViewModel> viewModel = getRatingViewModel();           
            return viewModel;
        }
    }
}