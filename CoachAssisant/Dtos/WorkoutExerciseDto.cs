﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoachAssisant.Dtos
{
    public class WorkoutExerciseDto
    {
        public int ClientId { get; set; }
        public List<int> ExercisesIds { get; set; }
    }
}