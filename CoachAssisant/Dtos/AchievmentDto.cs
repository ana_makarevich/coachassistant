﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Dtos
{
    public class AchievmentDto
    {
        [Required]
        public int Id { get; set; }
        
        public int? RatingPosition { get; set; }

        public int? yearmonth { get; set; }

        public int? yearweek { get; set; }

        public int? PreviousRatingPosition { get; set; }
        
        public byte CategoryID { get; set; }

        public CategoryDto Category { get; set; }
        
        public ClientDto Client { get; set; }
        [Required(ErrorMessage = "Поле Клиент обязательно")]
        public int ClientID { get; set; }

        public string Description { get; set; }

        [Required(ErrorMessage = "Дата обязательна")]
        public DateTime Added { get; set; }
    }
}