﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Dtos
{
    public class ExerciseDto
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Название обязательно")]
        [StringLength(255, ErrorMessage = "Длина названия не может превышать 255 символов")]        
        public string Name { get; set; }
        
        public string AlternativeName { get; set; }

        public string TargetMuscles { get; set; }
    }
}