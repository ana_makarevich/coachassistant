﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Dtos
{
    public class WorkoutDto
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Поле Клиент обязательно")]
        public int ClientID { get; set; }
        
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Статус тренировки обязателен")]
        public byte? WorkoutStatusID { get; set; }

        [Required(ErrorMessage = "Тип тренировки обязателен")]
        public byte? WorkoutTypeID { get; set; }
        
        public short? WorkoutNumber { get; set; }
    }
}