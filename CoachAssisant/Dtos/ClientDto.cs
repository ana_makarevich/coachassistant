﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoachAssisant.Dtos
{
    public class ClientDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    
        public string Email { get; set; }        
        public int WorkoutsCount { get; set; }
        
        public bool IsActive { get; set; }
        
        public int Reputation { get; set; }
        public int? CoachId { get; set; }

        public string FullName
        {
            get { return LastName + " " + FirstName; }
        }

    }
}