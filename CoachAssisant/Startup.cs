﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CoachAssisant.Startup))]
namespace CoachAssisant
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
