﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class Category
    {
        [Required]
        public int Id { get; set; }     
        
        [StringLength(255, ErrorMessage = "Длина названия не может превышать 255 символов")]
        [Required(ErrorMessage = "Название обязательно")]
        [Display(Name = nameof(Resources.DisplayNames.Name), 
            ResourceType = typeof(Resources.DisplayNames))]
        public string Name { get; set; }

        [Range(0.0, 1.0, ErrorMessage = "Значение должно быть больше 0 и меньше 1")]
        [Display(Name = nameof(Resources.DisplayNames.Weight), 
            ResourceType = typeof(Resources.DisplayNames))]
        public float? Weight { get; set; }
    }
}