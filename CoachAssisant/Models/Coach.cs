﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class Coach
    {
        [Required]
        public int Id { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.ApplicationUseer),
            ResourceType = typeof(Resources.DisplayNames))]
        public ApplicationUser ApplicationUser { get; set; }

        [StringLength(255, ErrorMessage = "Длина имени не может превышать 255 символов")]
        [Display(Name = nameof(Resources.DisplayNames.FirstName),
            ResourceType = typeof(Resources.DisplayNames))]
        public string FirstName { get; set; }

        [StringLength(255, ErrorMessage = "Длина фамилии не может превышать 255 символов")]
        [Display(Name = nameof(Resources.DisplayNames.LastName),
            ResourceType = typeof(Resources.DisplayNames))]
        public string LastName { get; set; }

        public Club Club { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.ClubName),
            ResourceType = typeof(Resources.DisplayNames))]
        public int? ClubId { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = nameof(Resources.DisplayNames.Email),
            ResourceType = typeof(Resources.DisplayNames))]
        public string Email { get; set; }


    }
}
 