﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoachAssisant.Models
{
    public class Achievment
    {
        [Required]
        public int Id { get; set; }            

        [Display(Name = nameof(Resources.DisplayNames.RatingPosition), 
            ResourceType = typeof(Resources.DisplayNames))]
        public int? RatingPosition { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.yearmonth), 
            ResourceType = typeof(Resources.DisplayNames))]        
        public int? yearmonth { get; set; }

        [Display(Name ="Год и неделя (ггггннн)")]
        public int? yearweek { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.PreviousRatingPosition), 
            ResourceType = typeof(Resources.DisplayNames))]        
        public int? PreviousRatingPosition { get; set; }
        
        public Category Category { get; set; }                

        [Display(Name = nameof(Resources.DisplayNames.Category), 
            ResourceType = typeof(Resources.DisplayNames))]
        public int CategoryId { get; set; }        

        public Client Client { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.Client), 
            ResourceType = typeof(Resources.DisplayNames))]
        [Required(ErrorMessage = "Поле Клиент обязательно")]
        public int ClientId { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.Notes), 
            ResourceType = typeof(Resources.DisplayNames))]
        public string Description { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.Added), ResourceType = typeof(Resources.DisplayNames))]
        [Required(ErrorMessage = "Дата обязательна")]
        public DateTime Added { get; set; }
    }
}