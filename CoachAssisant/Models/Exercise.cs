﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class Exercise
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Название обязательно")]
        [StringLength(255, ErrorMessage = "Длина названия не может превышать 255 символов")]
        [Display(Name = nameof(Resources.DisplayNames.Name),
            ResourceType = typeof(Resources.DisplayNames))]
        public string Name { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.AlternativeName),
            ResourceType = typeof(Resources.DisplayNames))]
        public string AlternativeName { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.TargetMuscles),
            ResourceType = typeof(Resources.DisplayNames))]
        public string TargetMuscles { get; set; }


    }

}