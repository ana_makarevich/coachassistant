﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class WorkoutStatus
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        [Display(Name = nameof(Resources.DisplayNames.Name),
            ResourceType = typeof(Resources.DisplayNames))]
        public string Name { get; set; }
        [StringLength(512)]
        [Display(Name = nameof(Resources.DisplayNames.Description),
            ResourceType = typeof(Resources.DisplayNames))]
        public string Description { get; set; }

        public static readonly byte Unknown = 0;
        public static readonly byte Completed = 1;
        public static readonly byte Planned = 2;
        public static readonly byte Cancelled = 3;
        
    }
}