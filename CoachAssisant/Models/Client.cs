﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class Client
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Имя обязательно")]
        [StringLength(255, ErrorMessage = "Длина имени не может превышать 255 символов")]
        [Display(Name = nameof(Resources.DisplayNames.FirstName), 
            ResourceType = typeof(Resources.DisplayNames))]
        public string FirstName { get; set; }

        [StringLength(255, ErrorMessage = "Длина фамилии не может превышать 255 символов")]
        [Display(Name = nameof(Resources.DisplayNames.LastName), 
            ResourceType = typeof(Resources.DisplayNames))]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = nameof(Resources.DisplayNames.Email), 
            ResourceType = typeof(Resources.DisplayNames))]        
        public string Email { get; set; }

        [Display(Name = "Дата рождения в виде ггггммдд")]
        public int? birthdate_yyyymmdd { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.BirthDate), 
            ResourceType = typeof(Resources.DisplayNames))]
        [IsAtLeast18]
        public DateTime BirthDate { get; set; }

        [Range(40,150, ErrorMessage = "Вес не может быть меньше 40 и больше 150")]
        [Display(Name = nameof(Resources.DisplayNames.Weight), 
            ResourceType = typeof(Resources.DisplayNames))]
        public float? Weight { get; set; }

        [Display(Name = "WorkoutsCount", ResourceType = typeof(Resources.DisplayNames))]
        public int WorkoutsCount { get; set; }

        [Required]
        [Display(Name = nameof(Resources.DisplayNames.IsActive),
            ResourceType = typeof(Resources.DisplayNames))]
        public bool IsActive { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.ApplicationUseer),
            ResourceType = typeof(Resources.DisplayNames))]
        public ApplicationUser ApplicationUser { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.Reputation),
            ResourceType = typeof(Resources.DisplayNames))]
        public int Reputation { get; set; }        

        public Coach Coach { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.Coach),
            ResourceType = typeof(Resources.DisplayNames))]
        public int? CoachId { get; set; }
        
        public string FullName
        {
            get { return LastName + " " + FirstName;  }
        }
    }
}