﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class Club
    {
        [Required]
        public int Id { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.ClubName),
            ResourceType = typeof(Resources.DisplayNames))]        
        public string ClubName { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.Address),
            ResourceType = typeof(Resources.DisplayNames))]
        public string Address { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = nameof(Resources.DisplayNames.Email),
            ResourceType = typeof(Resources.DisplayNames))]
        public string Email { get; set; }


    }
}