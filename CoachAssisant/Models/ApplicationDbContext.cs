﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{


    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        public DbSet<Client> Clients { get; set; }
        public DbSet<MeasurementRecord> MeasurementRecords { get; set; }
        public DbSet<MeasurementType> MeasurementTypes { get; set; }
        public DbSet<WorkoutStatus> WorkoutStatuses { get; set; }
        public DbSet<WorkoutType> WorkoutTypes { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Achievment> Achievments { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Exercise> Exercises { get; set; }

        public DbSet<WorkoutExercise> WorkoutExercises { get; set; }

        public DbSet<Club> Clubs { get; set; }
        public DbSet<ProfileType> ProfileTypes { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}