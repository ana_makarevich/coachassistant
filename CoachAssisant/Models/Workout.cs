﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class Workout
    {
        [Required]
        public int Id { get; set; }

        public Client Client { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.Client),
            ResourceType = typeof(Resources.DisplayNames))]
        [Required(ErrorMessage = "Поле Клиент обязательно")]
        public int ClientId { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.WorkoutDate),
            ResourceType = typeof(Resources.DisplayNames))]
        public DateTime Date { get; set; }        

        public WorkoutStatus WorkoutStatus { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.WorkoutStatus),
            ResourceType = typeof(Resources.DisplayNames))]
        [Required(ErrorMessage = "Статус тренировки обязателен")]
        public int? WorkoutStatusID { get; set; }
        
        public WorkoutType WokroutType { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.WorkoutType),
            ResourceType = typeof(Resources.DisplayNames))]
        [Required(ErrorMessage = "Тип тренировки обязателен")]
        public int? WorkoutTypeId { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.WorkoutNumber),
            ResourceType = typeof(Resources.DisplayNames))]
        public int? WorkoutNumber { get; set; }        

    }
}