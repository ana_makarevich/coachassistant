﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class MeasurementType
    {
        [Required]
        public byte Id { get; set; }

        [Required(ErrorMessage = "Название обязательно")]
        [StringLength(255, ErrorMessage = "Длина названия не может превышать 255 символов")]
        [Display(Name = nameof(Resources.DisplayNames.Name),
            ResourceType = typeof(Resources.DisplayNames))]
        public string Name { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.Description),
            ResourceType = typeof(Resources.DisplayNames))]
        public string Description { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.Unit),
            ResourceType = typeof(Resources.DisplayNames))]
        public string Unit { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.IsToBeMinimised),
            ResourceType = typeof(Resources.DisplayNames))]
        public Boolean IsToBeMinimised { get; set; }

        public static readonly byte Unknown = 0;
        public static readonly byte Forearm = 1;
        public static readonly byte Shoulder = 2;
        public static readonly byte Neck = 3;
        public static readonly byte Chest = 4;
        public static readonly byte Waist = 5;
        public static readonly byte Gluts = 6;
        public static readonly byte Hips = 7;
        public static readonly byte Shin = 9;



    }
}