﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class WorkoutExercise
    {


        [Required]
        public int Id { get; set; }
        
        public Exercise Exercise { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.Exercise),
            ResourceType = typeof(Resources.DisplayNames))]
        [Required]
        public int ExerciseId { get; set; }

        public Workout Workout { get; set; }
        
        [Display(Name = nameof(Resources.DisplayNames.Workout),
            ResourceType = typeof(Resources.DisplayNames))]
        [Required]
        public int WorkoutId { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.AlternativeName),
            ResourceType = typeof(Resources.DisplayNames))]
        public string AdditionalName { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.Wraps),
            ResourceType = typeof(Resources.DisplayNames))]
        public int? Wraps { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.WorkingWeight),
            ResourceType = typeof(Resources.DisplayNames))]
        public double? WorkingWeight { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.NumberOfSets),
            ResourceType = typeof(Resources.DisplayNames))]
        public int? NumberOfSets { get; set; }


    }
}