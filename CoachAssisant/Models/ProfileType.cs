﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace CoachAssisant.Models
{
    public class ProfileType
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = nameof(Resources.DisplayNames.Name),
            ResourceType = typeof(Resources.DisplayNames))]
        public string name { get; set; }

        public static readonly byte Unknown = 0;
        public static readonly byte Coach = 1;
        public static readonly byte Client = 2;
        public static readonly byte ClubAdmin = 3;


    }
}