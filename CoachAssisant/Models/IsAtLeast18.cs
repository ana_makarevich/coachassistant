﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class IsAtLeast18: ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            var client = (Client)validationContext.ObjectInstance;
            if (client.BirthDate == null)
            {
                return new ValidationResult("Дата рождения обязательна");
            }
            
            DateTime dateWhen18 = client.BirthDate.AddYears(18);
            bool moreThan18 = DateTime.Now > dateWhen18;
            return (moreThan18) 
                ? ValidationResult.Success 
                : new ValidationResult("Клиент должен быть старше 18");


        }
    }
}