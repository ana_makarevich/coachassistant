﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class WorkoutType
    {
        [Required]        
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        [Display(Name = nameof(Resources.DisplayNames.Name),
            ResourceType = typeof(Resources.DisplayNames))]
        public string Name { get; set; }
        [Display(Name = nameof(Resources.DisplayNames.Description),
            ResourceType = typeof(Resources.DisplayNames))]
        public string Description { get; set; }
    }
}