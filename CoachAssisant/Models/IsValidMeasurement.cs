﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class IsValidMeasurement: ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var measurement = (MeasurementRecord) validationContext.ObjectInstance;                        
            var smallSizes = new List<int>() {
                MeasurementType.Forearm,
                MeasurementType.Shoulder,
                MeasurementType.Neck,
                MeasurementType.Shin
            };
            bool valid = false;
            if (smallSizes.Contains(measurement.MeasurementTypeId)) {
                valid = measurement.Value < 100;    

            }
            else
            {
                valid = measurement.Value < 200;
            }
            return (valid) ? ValidationResult.Success 
                : new ValidationResult("Замер выходит за границы реальности");
            
        }
    }
}