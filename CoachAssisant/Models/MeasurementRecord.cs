﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class MeasurementRecord
    {
        [Required]        
        public long Id { get; set; }

        [Required(ErrorMessage = "Поле Клиент обязательно")]
        [Display(Name = nameof(Resources.DisplayNames.Client),
            ResourceType = typeof(Resources.DisplayNames))]
        public int ClientId { get; set; }
        
        public Client Client { get; set; }        
        
        public MeasurementType MeasurementType { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.MeasurementType),
            ResourceType = typeof(Resources.DisplayNames))]
        [Required(ErrorMessage = "Тип замера обязателен")]
        public int MeasurementTypeId { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.IsImprovement),
            ResourceType = typeof(Resources.DisplayNames))]
        public bool IsImprovment { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.MeasurementDate),
            ResourceType = typeof(Resources.DisplayNames))]
        public DateTime Date { get; set; }
        
        [Required(ErrorMessage = "Результат замера обязателен")]
        [Display(Name = nameof(Resources.DisplayNames.MeasurementValue),
            ResourceType = typeof(Resources.DisplayNames))]
        [IsValidMeasurement]
        public float Value { get; set; }
    }
}