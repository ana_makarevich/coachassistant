﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CoachAssisant.Models
{
    public class Payment
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Сумма обязательна")]
        [Display(Name = nameof(Resources.DisplayNames.Amount),
            ResourceType = typeof(Resources.DisplayNames))]
        public int? Amount { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.PaymentDate),
            ResourceType = typeof(Resources.DisplayNames))]
        public DateTime Date { get; set; }

        public Client Client { get; set; }

        [Required(ErrorMessage = "Поле Клиент обязательно")]
        [Display(Name = nameof(Resources.DisplayNames.Client),
            ResourceType = typeof(Resources.DisplayNames))]
        public int ClientId { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.OnTime),
            ResourceType = typeof(Resources.DisplayNames))]
        public bool OnTime { get; set; }

        [Display(Name = nameof(Resources.DisplayNames.Notes),
            ResourceType = typeof(Resources.DisplayNames))]
        public string Notes { get; set; }
    }
}