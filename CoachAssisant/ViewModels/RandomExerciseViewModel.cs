﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoachAssisant.Models.ViewModels
{
    public class RandomExerciseViewModel
    {
        public Exercise Exercise { get; set; }
        public List<Client> Clients { get; set; }
    }
}