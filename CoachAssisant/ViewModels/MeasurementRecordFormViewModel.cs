﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoachAssisant.Models;

namespace CoachAssisant.ViewModels
{
    public class MeasurementRecordFormViewModel
    {
        public IEnumerable<Client> Clients { get; set; }
        public IEnumerable<MeasurementType> MeasurementTypes { get; set; }
        public MeasurementRecord MeasurementRecord { get; set; }
        public string Title
        {
            get
            {
                if (MeasurementRecord != null && MeasurementRecord.Id != 0)
                    return "Изменить замер";
                return "Новый замер";
            }
        }
        public string ButtonText
        {
            get
            {
                if (MeasurementRecord != null && MeasurementRecord.Id != 0)
                    return "Изменить";
                return "Добавить";

            }
        }
    }
}