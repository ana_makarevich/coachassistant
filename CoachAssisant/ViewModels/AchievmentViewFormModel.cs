﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoachAssisant.Models;
namespace CoachAssisant.ViewModels
{
    public class AchievmentViewFormModel
    {
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Client> Clients { get; set; }
        public Achievment Achievment { get; set; }

        public string Title
        {
            get
            {
                if (Achievment != null && Achievment.Id != 0)
                    return "Изменить достижение";
                return "Новое достижение";
            }
        }
        public string ButtonText
        {
            get
            {
                if (Achievment != null && Achievment.Id != 0)
                    return "Изменить";
                return "Добавить";

            }
        }
    }
}