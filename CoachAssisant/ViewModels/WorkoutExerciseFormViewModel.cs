﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoachAssisant.Models;


namespace CoachAssisant.ViewModels
{
    public class WorkoutExerciseFormViewModel
    {
        public IEnumerable<Exercise> Exercises { get; set; }
        public WorkoutExercise WorkoutExercise { get; set; }
        public int WorkoutId { get; set; }

        public string Title
        {
            get
            {
                if (WorkoutExercise != null && WorkoutExercise.Id != 0)
                    return "Изменить упражнение";
                return "Новое упражнение";
            }
        }
        public string ButtonText
        {
            get
            {
                if (WorkoutExercise != null && WorkoutExercise.Id != 0)
                    return "Изменить";
                return "Добавить";

            }
        }

    }
}