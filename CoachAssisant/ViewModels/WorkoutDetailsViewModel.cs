﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoachAssisant.Models;

namespace CoachAssisant.ViewModels
{
    public class WorkoutDetailsViewModel
    {
        public IEnumerable<WorkoutExercise> WorkoutExercises { get; set; }
        public Workout Workout { get; set; }

        public string Title
        {
            get
            {                
                return "Тренировка";
            }
        }        
        }
    }
