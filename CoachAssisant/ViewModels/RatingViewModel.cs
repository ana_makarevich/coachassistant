﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoachAssisant.Models;

namespace CoachAssisant.ViewModels
{
    public class RatingViewModel
    {
        public Client Client { get; set; }        
        public float Value { get; set; }

    }
}