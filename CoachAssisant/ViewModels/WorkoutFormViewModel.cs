﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoachAssisant.Models;
namespace CoachAssisant.ViewModels
{
    public class WorkoutFormViewModel
    {
        public IEnumerable<WorkoutType> WorkoutTypes { get; set; }
        public IEnumerable<WorkoutStatus> WorkoutStatuses { get; set; }
        public IEnumerable<Client> Clients { get; set; }
        public Workout Workout { get; set; }

        public string Title
        {
            get
            {
                if (Workout != null && Workout.Id != 0)
                    return "Изменить тренировку";
                return "Новая тренировка";
            }
        }
        public string ButtonText
        {
            get
            {
                if (Workout != null && Workout.Id != 0)
                    return "Изменить";
                return "Добавить";
               
            }
        }
    }
}