﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoachAssisant.Models;

namespace CoachAssisant.ViewModels
{
    public class PaymentFormViewModel
    {
        public IEnumerable<Client> Clients { get; set; }
        public Payment Payment { get; set; }
        public string Title
        {
            get
            {
                if (Payment != null && Payment.Id != 0)
                    return "Изменить платеж";
                return "Новый платеж";
            }
        }
        public string ButtonText
        {
            get
            {
                if (Payment != null && Payment.Id != 0)
                    return "Изменить";
                return "Добавить";

            }
        }

    }
}