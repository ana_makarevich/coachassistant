﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using CoachAssisant.Models;

namespace CoachAssisant.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {

        
            [Required]
            [Display(Name = "Email")]
            public string Email { get; set; }
            [Display(Name = "Тип аккаунта")]
            [Required]
            public int ProfileTypeId { get; set; }

            public IEnumerable<ProfileType> ProfileTypes;

        
    }
}