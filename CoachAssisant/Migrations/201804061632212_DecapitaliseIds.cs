namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DecapitaliseIds : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Achievments", new[] { "CategoryID" });
            DropIndex("dbo.Achievments", new[] { "ClientID" });
            DropIndex("dbo.Clients", new[] { "CoachID" });
            DropIndex("dbo.AspNetUsers", new[] { "ProfileTypeID" });
            DropIndex("dbo.Coaches", new[] { "ClubID" });
            DropIndex("dbo.MeasurementRecords", new[] { "ClientID" });
            DropIndex("dbo.MeasurementRecords", new[] { "MeasurementTypeID" });
            DropIndex("dbo.Payments", new[] { "ClientID" });
            DropIndex("dbo.WorkoutExercises", new[] { "ExerciseID" });
            DropIndex("dbo.WorkoutExercises", new[] { "WorkoutID" });
            DropIndex("dbo.Workouts", new[] { "ClientID" });
            DropIndex("dbo.Workouts", new[] { "WorkoutTypeID" });
            CreateIndex("dbo.Achievments", "CategoryId");
            CreateIndex("dbo.Achievments", "ClientId");
            CreateIndex("dbo.Clients", "CoachId");
            CreateIndex("dbo.AspNetUsers", "ProfileTypeId");
            CreateIndex("dbo.Coaches", "ClubId");
            CreateIndex("dbo.MeasurementRecords", "ClientId");
            CreateIndex("dbo.MeasurementRecords", "MeasurementTypeId");
            CreateIndex("dbo.Payments", "ClientId");
            CreateIndex("dbo.WorkoutExercises", "ExerciseId");
            CreateIndex("dbo.WorkoutExercises", "WorkoutId");
            CreateIndex("dbo.Workouts", "ClientId");
            CreateIndex("dbo.Workouts", "WorkoutTypeId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Workouts", new[] { "WorkoutTypeId" });
            DropIndex("dbo.Workouts", new[] { "ClientId" });
            DropIndex("dbo.WorkoutExercises", new[] { "WorkoutId" });
            DropIndex("dbo.WorkoutExercises", new[] { "ExerciseId" });
            DropIndex("dbo.Payments", new[] { "ClientId" });
            DropIndex("dbo.MeasurementRecords", new[] { "MeasurementTypeId" });
            DropIndex("dbo.MeasurementRecords", new[] { "ClientId" });
            DropIndex("dbo.Coaches", new[] { "ClubId" });
            DropIndex("dbo.AspNetUsers", new[] { "ProfileTypeId" });
            DropIndex("dbo.Clients", new[] { "CoachId" });
            DropIndex("dbo.Achievments", new[] { "ClientId" });
            DropIndex("dbo.Achievments", new[] { "CategoryId" });
            CreateIndex("dbo.Workouts", "WorkoutTypeID");
            CreateIndex("dbo.Workouts", "ClientID");
            CreateIndex("dbo.WorkoutExercises", "WorkoutID");
            CreateIndex("dbo.WorkoutExercises", "ExerciseID");
            CreateIndex("dbo.Payments", "ClientID");
            CreateIndex("dbo.MeasurementRecords", "MeasurementTypeID");
            CreateIndex("dbo.MeasurementRecords", "ClientID");
            CreateIndex("dbo.Coaches", "ClubID");
            CreateIndex("dbo.AspNetUsers", "ProfileTypeID");
            CreateIndex("dbo.Clients", "CoachID");
            CreateIndex("dbo.Achievments", "ClientID");
            CreateIndex("dbo.Achievments", "CategoryID");
        }
    }
}
