namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTargetMusclesFieldToExercises : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Exercises", "TargetMuscles", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Exercises", "TargetMuscles");
        }
    }
}
