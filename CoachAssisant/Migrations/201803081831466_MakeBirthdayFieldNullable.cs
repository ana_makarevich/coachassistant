namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeBirthdayFieldNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Clients", "birthdate_yyyymmdd", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Clients", "birthdate_yyyymmdd", c => c.Int(nullable: false));
        }
    }
}
