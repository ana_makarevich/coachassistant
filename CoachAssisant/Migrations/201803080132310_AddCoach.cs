namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCoach : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Coaches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationUserID = c.Int(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            AddColumn("dbo.Clients", "Reputation", c => c.Int(nullable: false));
            AddColumn("dbo.Clients", "CoachID", c => c.Int(nullable: false));
            CreateIndex("dbo.Clients", "CoachID");
            AddForeignKey("dbo.Clients", "CoachID", "dbo.Coaches", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Clients", "CoachID", "dbo.Coaches");
            DropForeignKey("dbo.Coaches", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Coaches", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.Clients", new[] { "CoachID" });
            DropColumn("dbo.Clients", "CoachID");
            DropColumn("dbo.Clients", "Reputation");
            DropTable("dbo.Coaches");
        }
    }
}
