namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUnitFieldToMeasurementType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MeasurementTypes", "Unit", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MeasurementTypes", "Unit");
        }
    }
}
