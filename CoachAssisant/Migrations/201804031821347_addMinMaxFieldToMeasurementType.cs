namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMinMaxFieldToMeasurementType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MeasurementTypes", "IsToBeMinimised", c => c.Boolean(nullable: false, defaultValue: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MeasurementTypes", "IsToBeMinimised");
        }
    }
}
