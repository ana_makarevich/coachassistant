﻿namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMeasurementTypes : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO MeasurementTypes (Id, Name, Description)" +
              "VALUES (1, N'Предплечье', N'Замерять в самом толстом месте. В напряженном состоянии – кисть сильно сжата в кулак, локоть согнут до 90 градусов. В расслабленном состоянии – прямая рука спокойно висит вдоль тела.')");           
        }
        
        public override void Down()
        {
        }
    }
}
