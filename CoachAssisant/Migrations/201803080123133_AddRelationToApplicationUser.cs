namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRelationToApplicationUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "ApplicationUserId", c => c.Int(nullable: false));
            AddColumn("dbo.Clients", "ApplicationUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Clients", "ApplicationUser_Id");
            AddForeignKey("dbo.Clients", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Clients", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Clients", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.Clients", "ApplicationUser_Id");
            DropColumn("dbo.Clients", "ApplicationUserId");
        }
    }
}
