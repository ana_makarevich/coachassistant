namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeWorkingWeightDataType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.WorkoutExercises", "WorkingWeight", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.WorkoutExercises", "WorkingWeight", c => c.Int());
        }
    }
}
