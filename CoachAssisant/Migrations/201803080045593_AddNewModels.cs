namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Workouts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientID = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        WorkoutStatusID = c.Byte(nullable: false),
                        WorkoutNumber = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientID, cascadeDelete: true)
                .ForeignKey("dbo.WorkoutStatus", t => t.WorkoutStatusID, cascadeDelete: true)
                .Index(t => t.ClientID)
                .Index(t => t.WorkoutStatusID);
            
            CreateTable(
                "dbo.WorkoutStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(nullable: false, maxLength: 255),
                        Description = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkoutTypes",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(nullable: false, maxLength: 255),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Workouts", "WorkoutStatusID", "dbo.WorkoutStatus");
            DropForeignKey("dbo.Workouts", "ClientID", "dbo.Clients");
            DropIndex("dbo.Workouts", new[] { "WorkoutStatusID" });
            DropIndex("dbo.Workouts", new[] { "ClientID" });
            DropTable("dbo.WorkoutTypes");
            DropTable("dbo.WorkoutStatus");
            DropTable("dbo.Workouts");
        }
    }
}
