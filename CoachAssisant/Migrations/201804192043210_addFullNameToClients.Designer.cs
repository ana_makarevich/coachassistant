// <auto-generated />
namespace CoachAssisant.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addFullNameToClients : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addFullNameToClients));
        
        string IMigrationMetadata.Id
        {
            get { return "201804192043210_addFullNameToClients"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
