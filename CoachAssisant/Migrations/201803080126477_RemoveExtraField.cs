namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveExtraField : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Clients", "ApplicationUserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Clients", "ApplicationUserId", c => c.Int(nullable: false));
        }
    }
}
