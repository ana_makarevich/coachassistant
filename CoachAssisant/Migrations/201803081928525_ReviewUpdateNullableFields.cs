namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReviewUpdateNullableFields : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Workouts", "WorkoutStatusID", "dbo.WorkoutStatus");
            DropIndex("dbo.Workouts", new[] { "WorkoutStatusID" });
            CreateTable(
                "dbo.Exercises",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        AlternativeName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Achievments", "Description", c => c.String());
            AlterColumn("dbo.Achievments", "RatingPosition", c => c.Int());
            AlterColumn("dbo.Achievments", "yearmonth", c => c.Int());
            AlterColumn("dbo.Achievments", "yearweek", c => c.Int());
            AlterColumn("dbo.Achievments", "PreviousRatingPosition", c => c.Int());
            AlterColumn("dbo.Categories", "Weight", c => c.Single());
            AlterColumn("dbo.Clients", "Weight", c => c.Single());
            AlterColumn("dbo.Payments", "Amount", c => c.Int());
            AlterColumn("dbo.Workouts", "WorkoutStatusID", c => c.Byte());
            AlterColumn("dbo.Workouts", "WorkoutNumber", c => c.Short());
            CreateIndex("dbo.Workouts", "WorkoutStatusID");
            AddForeignKey("dbo.Workouts", "WorkoutStatusID", "dbo.WorkoutStatus", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Workouts", "WorkoutStatusID", "dbo.WorkoutStatus");
            DropIndex("dbo.Workouts", new[] { "WorkoutStatusID" });
            AlterColumn("dbo.Workouts", "WorkoutNumber", c => c.Short(nullable: false));
            AlterColumn("dbo.Workouts", "WorkoutStatusID", c => c.Byte(nullable: false));
            AlterColumn("dbo.Payments", "Amount", c => c.Int(nullable: false));
            AlterColumn("dbo.Clients", "Weight", c => c.Single(nullable: false));
            AlterColumn("dbo.Categories", "Weight", c => c.Single(nullable: false));
            AlterColumn("dbo.Achievments", "PreviousRatingPosition", c => c.Int(nullable: false));
            AlterColumn("dbo.Achievments", "yearweek", c => c.Int(nullable: false));
            AlterColumn("dbo.Achievments", "yearmonth", c => c.Int(nullable: false));
            AlterColumn("dbo.Achievments", "RatingPosition", c => c.Int(nullable: false));
            DropColumn("dbo.Achievments", "Description");
            DropTable("dbo.Exercises");
            CreateIndex("dbo.Workouts", "WorkoutStatusID");
            AddForeignKey("dbo.Workouts", "WorkoutStatusID", "dbo.WorkoutStatus", "Id", cascadeDelete: true);
        }
    }
}
