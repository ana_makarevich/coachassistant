namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddValueFieldToMeasurement : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MeasurementRecords", "Value", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MeasurementRecords", "Value");
        }
    }
}
