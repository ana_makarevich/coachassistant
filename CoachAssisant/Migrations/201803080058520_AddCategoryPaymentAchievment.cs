namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategoryPaymentAchievment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Achievments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RatingPosition = c.Int(nullable: false),
                        yearmonth = c.Int(nullable: false),
                        yearweek = c.Int(nullable: false),
                        PreviousRatingPosition = c.Int(nullable: false),
                        CategoryID = c.Byte(nullable: false),
                        ClientID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: true)
                .ForeignKey("dbo.Clients", t => t.ClientID, cascadeDelete: true)
                .Index(t => t.CategoryID)
                .Index(t => t.ClientID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(nullable: false, maxLength: 255),
                        Weight = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Amount = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        ClientID = c.Int(nullable: false),
                        OnTime = c.Boolean(nullable: false),
                        Notes = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientID, cascadeDelete: true)
                .Index(t => t.ClientID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Payments", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.Achievments", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.Achievments", "CategoryID", "dbo.Categories");
            DropIndex("dbo.Payments", new[] { "ClientID" });
            DropIndex("dbo.Achievments", new[] { "ClientID" });
            DropIndex("dbo.Achievments", new[] { "CategoryID" });
            DropTable("dbo.Payments");
            DropTable("dbo.Categories");
            DropTable("dbo.Achievments");
        }
    }
}
