﻿namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateCategories : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Categories (Id, Name, Weight)" +
                "VALUES (1, N'Уменьшение объемов', N'0.4')");
            Sql("INSERT INTO Categories (Id, Name, Weight)" +
                "VALUES (2, N'Уменьшение веса', N'0.3')");
            Sql("INSERT INTO Categories (Id, Name, Weight)" +
                "VALUES (3, N'Правильное питание', N'0.2')");
            Sql("INSERT INTO Categories (Id, Name, Weight)" +
                "VALUES (4, N'Постоянство', N'0.1')");
        }
        
        public override void Down()
        {
        }
    }
}
