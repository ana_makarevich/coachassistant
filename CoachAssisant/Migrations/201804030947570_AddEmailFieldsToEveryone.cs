namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailFieldsToEveryone : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "Email", c => c.String(nullable: false));
            AddColumn("dbo.Coaches", "Email", c => c.String(nullable: false));
            AddColumn("dbo.Clubs", "Email", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clubs", "Email");
            DropColumn("dbo.Coaches", "Email");
            DropColumn("dbo.Clients", "Email");
        }
    }
}
