namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeCoachIDNullable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Clients", "CoachID", "dbo.Coaches");
            DropIndex("dbo.Clients", new[] { "CoachID" });
            AlterColumn("dbo.Clients", "CoachID", c => c.Int());
            CreateIndex("dbo.Clients", "CoachID");
            AddForeignKey("dbo.Clients", "CoachID", "dbo.Coaches", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Clients", "CoachID", "dbo.Coaches");
            DropIndex("dbo.Clients", new[] { "CoachID" });
            AlterColumn("dbo.Clients", "CoachID", c => c.Int(nullable: false));
            CreateIndex("dbo.Clients", "CoachID");
            AddForeignKey("dbo.Clients", "CoachID", "dbo.Coaches", "Id", cascadeDelete: true);
        }
    }
}
