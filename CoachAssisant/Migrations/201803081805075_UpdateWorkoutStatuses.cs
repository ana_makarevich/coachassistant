﻿namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateWorkoutStatuses : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE WorkoutStatus " +
                "SET Description=N'Пропуск тренировки'" +
                "WHERE Name=N'Отменена'");
        }
        
        public override void Down()
        {
        }
    }
}
