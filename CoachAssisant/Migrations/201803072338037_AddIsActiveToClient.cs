namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsActiveToClient : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clients", "IsActive");
        }
    }
}
