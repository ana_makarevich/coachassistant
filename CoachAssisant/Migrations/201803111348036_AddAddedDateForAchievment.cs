namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAddedDateForAchievment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Achievments", "Added", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));

        }
        
        public override void Down()
        {
            DropColumn("dbo.Achievments", "Added");
        }
    }
}
