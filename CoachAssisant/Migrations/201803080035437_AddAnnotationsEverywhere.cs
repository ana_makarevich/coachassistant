﻿namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAnnotationsEverywhere : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MeasurementTypes", "Name", c => c.String(nullable: false, maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MeasurementTypes", "Name", c => c.String());
        }
    }
}
