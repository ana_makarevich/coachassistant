namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'daf0e894-1e39-4091-9b7c-a24f5ed73880', N'guest@coachassistant.com', 0, N'AByx9TSSYAD/y4vGC8RbEMzHdMM9NF3Sj/nNKewwuw63aYWIQpTSYnyE1B+GB7o7aA==', N'f5ca3ffd-dc83-4e67-ab5b-5c1b61d39dbd', NULL, 0, 0, NULL, 1, 0, N'guest@coachassistant.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e4286545-e541-48f9-b86d-e334f405d442', N'maincoach@coachassistant.com', 0, N'ABVqAxdRSkNEVfomPAy+wHCeHQiOn+H6HKW2RUjaD1tOuDcfILgk7F4jK4mJEwXLig==', N'29803fa3-afb3-48f7-91e7-ac0037afa3bf', NULL, 0, 0, NULL, 1, 0, N'maincoach@coachassistant.com')
INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'6be98ebf-c005-4a1b-be88-fd4d619afa91', N'CanManageClients')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e4286545-e541-48f9-b86d-e334f405d442', N'6be98ebf-c005-4a1b-be88-fd4d619afa91')
");
        }
        
        public override void Down()
        {
        }
    }
}
