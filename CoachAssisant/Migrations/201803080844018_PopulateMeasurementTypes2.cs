﻿namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMeasurementTypes2 : DbMigration
    {
        public override void Up()
        {            
            Sql("INSERT INTO MeasurementTypes (Id, Name, Description)" +
                "VALUES (2, N'Плечо', N'Замерять в самом толстом месте. В напряженном состоянии – рука согнута в локте, кулак поднят вверх около головы.В расслабленном состоянии – прямая рука спокойно висит вдоль тела.')");
            Sql("INSERT INTO MeasurementTypes (Id, Name, Description)" +
                "VALUES (3, N'Шея', N'Шею обычно замеряют в спокойном состоянии, у основания. Не направляйте ленту наискось, а замеряйте горизонтальную окружность.')");
            Sql("INSERT INTO MeasurementTypes (Id, Name, Description)" +
                "VALUES (4, N'Грудная клетка', N'Замерять в самом объемном месте.')");
            Sql("INSERT INTO MeasurementTypes (Id, Name, Description)" +
                "VALUES (5, N'Талия', N'Тем у кого есть талия – замерять в самом тонком месте. Если у вас пока еще выступающий живот, то его и замеряем, примерно в середине живота в самом толстом месте.')");
            Sql("INSERT INTO MeasurementTypes (Id, Name, Description)" +
                "VALUES (6, N'Ягодицы', N'Замерять по наиболее выступающей линии попы. Обычно замеряют стоя, в спокойном состоянии.')");
            Sql("INSERT INTO MeasurementTypes (Id, Name, Description)" +
                "VALUES (7, N'Бедро', N'Замерять в самом толстом месте (примерно в верхней трети бедра). В напряженном состоянии – нога выпрямлена, мышцы напряжены. В расслабленном состоянии – стоя, или согнутую ногу ставим на стул.')");
            Sql("INSERT INTO MeasurementTypes (Id, Name, Description)" +
                "VALUES (8, N'Голень', N'Замерять в самом толстом месте. В напряженном состоянии – приподняться на носок и нагрузить весом. В расслабленном состоянии – согнутую ногу поставить на стул')");
        }
        
        public override void Down()
        {
        }
    }
}
