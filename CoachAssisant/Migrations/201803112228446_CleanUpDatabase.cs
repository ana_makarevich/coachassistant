namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CleanUpDatabase : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Workouts", "WorkoutTypeID", "dbo.WorkoutTypes");
            DropForeignKey("dbo.Workouts", "WorkoutStatusID", "dbo.WorkoutStatus");
            DropIndex("dbo.Workouts", new[] { "WorkoutStatusID" });
            DropIndex("dbo.Workouts", new[] { "WorkoutTypeID" });
            AlterColumn("dbo.Coaches", "FirstName", c => c.String(maxLength: 255));
            AlterColumn("dbo.Coaches", "LastName", c => c.String(maxLength: 255));
            AlterColumn("dbo.Exercises", "Name", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Payments", "Amount", c => c.Int(nullable: false));
            AlterColumn("dbo.Workouts", "WorkoutStatusID", c => c.Byte(nullable: false));
            AlterColumn("dbo.Workouts", "WorkoutTypeID", c => c.Byte(nullable: false));
            CreateIndex("dbo.Workouts", "WorkoutStatusID");
            CreateIndex("dbo.Workouts", "WorkoutTypeID");
            AddForeignKey("dbo.Workouts", "WorkoutTypeID", "dbo.WorkoutTypes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Workouts", "WorkoutStatusID", "dbo.WorkoutStatus", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Workouts", "WorkoutStatusID", "dbo.WorkoutStatus");
            DropForeignKey("dbo.Workouts", "WorkoutTypeID", "dbo.WorkoutTypes");
            DropIndex("dbo.Workouts", new[] { "WorkoutTypeID" });
            DropIndex("dbo.Workouts", new[] { "WorkoutStatusID" });
            AlterColumn("dbo.Workouts", "WorkoutTypeID", c => c.Byte());
            AlterColumn("dbo.Workouts", "WorkoutStatusID", c => c.Byte());
            AlterColumn("dbo.Payments", "Amount", c => c.Int());
            AlterColumn("dbo.Exercises", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Coaches", "LastName", c => c.String());
            AlterColumn("dbo.Coaches", "FirstName", c => c.String());
            CreateIndex("dbo.Workouts", "WorkoutTypeID");
            CreateIndex("dbo.Workouts", "WorkoutStatusID");
            AddForeignKey("dbo.Workouts", "WorkoutStatusID", "dbo.WorkoutStatus", "Id");
            AddForeignKey("dbo.Workouts", "WorkoutTypeID", "dbo.WorkoutTypes", "Id");
        }
    }
}
