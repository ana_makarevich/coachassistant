namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeByteToInt : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MeasurementRecords", "MeasurementTypeId", "dbo.MeasurementTypes");
            DropForeignKey("dbo.Achievments", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Workouts", "WorkoutTypeId", "dbo.WorkoutTypes");
            DropForeignKey("dbo.Workouts", "WorkoutStatusID", "dbo.WorkoutStatus");
            DropIndex("dbo.Achievments", new[] { "CategoryId" });
            DropIndex("dbo.MeasurementRecords", new[] { "MeasurementTypeId" });
            DropIndex("dbo.Workouts", new[] { "WorkoutStatusID" });
            DropIndex("dbo.Workouts", new[] { "WorkoutTypeId" });
            DropPrimaryKey("dbo.Categories");
            DropPrimaryKey("dbo.WorkoutTypes");
            DropPrimaryKey("dbo.WorkoutStatus");
            AddColumn("dbo.MeasurementRecords", "MeasurementType_Id", c => c.Byte());
            AlterColumn("dbo.Achievments", "CategoryId", c => c.Int(nullable: false));
            AlterColumn("dbo.Categories", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.MeasurementRecords", "MeasurementTypeId", c => c.Int(nullable: false));
            AlterColumn("dbo.Workouts", "WorkoutStatusID", c => c.Int(nullable: false));
            AlterColumn("dbo.Workouts", "WorkoutTypeId", c => c.Int(nullable: false));
            AlterColumn("dbo.Workouts", "WorkoutNumber", c => c.Int());
            AlterColumn("dbo.WorkoutTypes", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.WorkoutStatus", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Categories", "Id");
            AddPrimaryKey("dbo.WorkoutTypes", "Id");
            AddPrimaryKey("dbo.WorkoutStatus", "Id");
            CreateIndex("dbo.Achievments", "CategoryId");
            CreateIndex("dbo.MeasurementRecords", "MeasurementType_Id");
            CreateIndex("dbo.Workouts", "WorkoutStatusID");
            CreateIndex("dbo.Workouts", "WorkoutTypeId");
            AddForeignKey("dbo.MeasurementRecords", "MeasurementType_Id", "dbo.MeasurementTypes", "Id");
            AddForeignKey("dbo.Achievments", "CategoryId", "dbo.Categories", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Workouts", "WorkoutTypeId", "dbo.WorkoutTypes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Workouts", "WorkoutStatusID", "dbo.WorkoutStatus", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Workouts", "WorkoutStatusID", "dbo.WorkoutStatus");
            DropForeignKey("dbo.Workouts", "WorkoutTypeId", "dbo.WorkoutTypes");
            DropForeignKey("dbo.Achievments", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.MeasurementRecords", "MeasurementType_Id", "dbo.MeasurementTypes");
            DropIndex("dbo.Workouts", new[] { "WorkoutTypeId" });
            DropIndex("dbo.Workouts", new[] { "WorkoutStatusID" });
            DropIndex("dbo.MeasurementRecords", new[] { "MeasurementType_Id" });
            DropIndex("dbo.Achievments", new[] { "CategoryId" });
            DropPrimaryKey("dbo.WorkoutStatus");
            DropPrimaryKey("dbo.WorkoutTypes");
            DropPrimaryKey("dbo.Categories");
            AlterColumn("dbo.WorkoutStatus", "Id", c => c.Byte(nullable: false));
            AlterColumn("dbo.WorkoutTypes", "Id", c => c.Byte(nullable: false));
            AlterColumn("dbo.Workouts", "WorkoutNumber", c => c.Short());
            AlterColumn("dbo.Workouts", "WorkoutTypeId", c => c.Byte(nullable: false));
            AlterColumn("dbo.Workouts", "WorkoutStatusID", c => c.Byte(nullable: false));
            AlterColumn("dbo.MeasurementRecords", "MeasurementTypeId", c => c.Byte(nullable: false));
            AlterColumn("dbo.Categories", "Id", c => c.Byte(nullable: false));
            AlterColumn("dbo.Achievments", "CategoryId", c => c.Byte(nullable: false));
            DropColumn("dbo.MeasurementRecords", "MeasurementType_Id");
            AddPrimaryKey("dbo.WorkoutStatus", "Id");
            AddPrimaryKey("dbo.WorkoutTypes", "Id");
            AddPrimaryKey("dbo.Categories", "Id");
            CreateIndex("dbo.Workouts", "WorkoutTypeId");
            CreateIndex("dbo.Workouts", "WorkoutStatusID");
            CreateIndex("dbo.MeasurementRecords", "MeasurementTypeId");
            CreateIndex("dbo.Achievments", "CategoryId");
            AddForeignKey("dbo.Workouts", "WorkoutStatusID", "dbo.WorkoutStatus", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Workouts", "WorkoutTypeId", "dbo.WorkoutTypes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Achievments", "CategoryId", "dbo.Categories", "Id", cascadeDelete: true);
            AddForeignKey("dbo.MeasurementRecords", "MeasurementTypeId", "dbo.MeasurementTypes", "Id", cascadeDelete: true);
        }
    }
}
