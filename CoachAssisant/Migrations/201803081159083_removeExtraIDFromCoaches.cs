namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeExtraIDFromCoaches : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Coaches", "ApplicationUserID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Coaches", "ApplicationUserID", c => c.Int(nullable: false));
        }
    }
}
