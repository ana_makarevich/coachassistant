﻿namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateWorkoutStatuses : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO WorkoutStatus (Id, Name, Description)" +
                "VALUES (1, N'Проведена', '')");
            Sql("INSERT INTO WorkoutStatus (Id, Name, Description)" +
                "VALUES (2, N'Запланирована', '')");
            Sql("INSERT INTO WorkoutStatus (Id, Name, Description)" +
                "VALUES (3, N'Отменена', '')");
        }
        
        public override void Down()
        {
        }
    }
}
