namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMeasurementType2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MeasurementRecords",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ClientID = c.Int(nullable: false),
                        MeasurementTypeID = c.Byte(nullable: false),
                        IsImprovment = c.Boolean(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientID, cascadeDelete: true)
                .ForeignKey("dbo.MeasurementTypes", t => t.MeasurementTypeID, cascadeDelete: true)
                .Index(t => t.ClientID)
                .Index(t => t.MeasurementTypeID);
            
            CreateTable(
                "dbo.MeasurementTypes",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MeasurementRecords", "MeasurementTypeID", "dbo.MeasurementTypes");
            DropForeignKey("dbo.MeasurementRecords", "ClientID", "dbo.Clients");
            DropIndex("dbo.MeasurementRecords", new[] { "MeasurementTypeID" });
            DropIndex("dbo.MeasurementRecords", new[] { "ClientID" });
            DropTable("dbo.MeasurementTypes");
            DropTable("dbo.MeasurementRecords");
        }
    }
}
