﻿namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateWorkoutType : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO WorkoutTypes (Id, Name, Description)" +
               "VALUES (1, N'Ноги', '')");
            Sql("INSERT INTO WorkoutTypes (Id, Name, Description)" +
               "VALUES (2, N'Грудь-Спина', '')");
            Sql("INSERT INTO WorkoutTypes (Id, Name, Description)" +
               "VALUES (3, N'Руки-Плечи', '')");
            Sql("INSERT INTO WorkoutTypes (Id, Name, Description)" +
               "VALUES (4, N'Ноги-Плечи', '')");
            Sql("INSERT INTO WorkoutTypes (Id, Name, Description)" +
               "VALUES (5, N'Спина-Трицепс', '')");
            Sql("INSERT INTO WorkoutTypes (Id, Name, Description)" +
               "VALUES (6, N'Грудь-Бицепс', '')");

        }
        
        public override void Down()
        {
        }
    }
}
