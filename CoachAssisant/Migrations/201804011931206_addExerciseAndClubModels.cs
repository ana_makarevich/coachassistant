namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addExerciseAndClubModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clubs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClubName = c.String(),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkoutExercises",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExerciseID = c.Int(nullable: false),
                        WorkoutID = c.Int(nullable: false),
                        AdditionalName = c.String(),
                        Wraps = c.Int(),
                        WorkingWeight = c.Int(),
                        NumberOfSets = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Exercises", t => t.ExerciseID, cascadeDelete: true)
                .ForeignKey("dbo.Workouts", t => t.WorkoutID, cascadeDelete: true)
                .Index(t => t.ExerciseID)
                .Index(t => t.WorkoutID);
            
            AddColumn("dbo.Coaches", "ClubID", c => c.Int());
            CreateIndex("dbo.Coaches", "ClubID");
            AddForeignKey("dbo.Coaches", "ClubID", "dbo.Clubs", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkoutExercises", "WorkoutID", "dbo.Workouts");
            DropForeignKey("dbo.WorkoutExercises", "ExerciseID", "dbo.Exercises");
            DropForeignKey("dbo.Coaches", "ClubID", "dbo.Clubs");
            DropIndex("dbo.WorkoutExercises", new[] { "WorkoutID" });
            DropIndex("dbo.WorkoutExercises", new[] { "ExerciseID" });
            DropIndex("dbo.Coaches", new[] { "ClubID" });
            DropColumn("dbo.Coaches", "ClubID");
            DropTable("dbo.WorkoutExercises");
            DropTable("dbo.Clubs");
        }
    }
}
