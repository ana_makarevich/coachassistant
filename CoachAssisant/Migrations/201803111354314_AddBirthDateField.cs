namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBirthDateField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "BirthDate", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clients", "BirthDate");
        }
    }
}
