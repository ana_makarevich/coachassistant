namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProfileTypeField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "ProfileTypeID", c => c.Int(nullable: false, defaultValue: 2));
            CreateIndex("dbo.AspNetUsers", "ProfileTypeID");
            AddForeignKey("dbo.AspNetUsers", "ProfileTypeID", "dbo.ProfileTypes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "ProfileTypeID", "dbo.ProfileTypes");
            DropIndex("dbo.AspNetUsers", new[] { "ProfileTypeID" });
            DropColumn("dbo.AspNetUsers", "ProfileTypeID");
        }
    }
}
