﻿namespace CoachAssisant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateProfileType : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO ProfileTypes (Name)" +
                "VALUES (N'Тренер')");
            Sql("INSERT INTO ProfileTypes (Name)" +
                "VALUES (N'Клиент')");
            Sql("INSERT INTO ProfileTypes (Name)" +
                "VALUES (N'АдминЗала')");

        }
        
        public override void Down()
        {
        }
    }
}
