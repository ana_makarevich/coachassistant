﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CoachAssisant
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            /*                         
                        routes.MapRoute(
                            name: "WorkoutsByDate",
                            url: "api/workouts/list/{year}/{month}",
                            defaults: new { controller = "Workouts", action = "ByDate"},
                            constraints: new { year = @"\d{4}", month = @"\d{2}"}
                            );            

                        */
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                // id is not needed so we set it here as optional
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }
    }
}
