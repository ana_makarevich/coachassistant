﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using CoachAssisant.Models;
using CoachAssisant.Dtos;

namespace CoachAssisant.App_Start
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<Exercise, ExerciseDto>();            
            Mapper.CreateMap<Workout, WorkoutDto>();            
            Mapper.CreateMap<Achievment, AchievmentDto>();
            Mapper.CreateMap<Client, ClientDto>();
            Mapper.CreateMap<Category, CategoryDto>();


            Mapper.CreateMap<ExerciseDto, Exercise>();
            Mapper.CreateMap<WorkoutDto, Workout>().ForMember(c => c.Id, opt => opt.Ignore());
            Mapper.CreateMap<AchievmentDto, Achievment>().ForMember(c => c.Id, opt => opt.Ignore());

        }
    }
}